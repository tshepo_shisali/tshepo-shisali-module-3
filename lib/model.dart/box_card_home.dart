import 'package:flutter/material.dart';
import 'package:med_it/model.dart/product.dart';

class Category extends StatelessWidget {
  final Product product;
  final VoidCallback press;
  
  const Category({ Key? key,required this.product,required this.press }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: press,
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children:<Widget> [
        Expanded(
          child: Container(
            padding: EdgeInsets.all(20.0),
            decoration: BoxDecoration(
                    color: Theme.of(context).primaryColor,
                    borderRadius: BorderRadius.circular(16),
                  ),
          
          
          child: Hero(
            tag: "${product.id}",
            child: Image.asset(product.image)),
        ),
        ),
        Padding(
          padding: const EdgeInsets.symmetric(
            vertical: 5.0,
            
          ),
          
          child: Text(
            product.name,
            style: Theme.of(context).textTheme.headline4,
          )
          ),

        ]
      ),
    );
  }
}
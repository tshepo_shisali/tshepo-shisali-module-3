// ignore_for_file: prefer_const_constructors, prefer_const_literals_to_create_immutables

import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:med_it/screens/login.dart';
import 'package:med_it/screens/profile_widget/address_book.dart';
import 'package:med_it/screens/profile_widget/help.dart';
import 'package:med_it/screens/profile_widget/orders.dart';
import 'package:med_it/screens/profile_widget/patient_file.dart';
import 'package:med_it/screens/profile_widget/personal_details.dart';
import 'package:med_it/screens/profile_widget/profile_pic.dart';
import 'package:med_it/screens/profile_widget/returns.dart';
import 'package:med_it/screens/profile_widget/settings.dart';

class Account extends StatefulWidget {
  const Account({ Key? key }) : super(key: key);

  @override
  State<Account> createState() => _AccountState();
}

class _AccountState extends State<Account> {
  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.center,
      children:  [
        ProfilePic(),
        SizedBox(height: 20),
        
        ProfileMenu(
          icon: Icons.person, 
          text: 'Profile', 
          press: () {
             Navigator.push(context,
             MaterialPageRoute(builder: (context)=> Personal_details()));
           },
          ),
          ProfileMenu(
          icon: Icons.file_copy_outlined,
          text: 'My file', 
          press: () { 
             Navigator.push(context,
             MaterialPageRoute(builder: (context)=> My_file()));
          },
          ),
          ProfileMenu(
          icon: Icons.shopping_cart, 
          text: 'orders', 
          press: () { 
             Navigator.push(context,
             MaterialPageRoute(builder: (context)=> Orders()));
          },
          ),
          ProfileMenu(
          icon: Icons.shopping_bag, 
          text: 'Returns', 
          press: () { 
             Navigator.push(context,
             MaterialPageRoute(builder: (context)=> Returns()));
          },
          ),
          ProfileMenu(
          icon: Icons.book, 
          text: 'adress book', 
          press: () {
             Navigator.push(context,
             MaterialPageRoute(builder: (context)=> Address_book()));
           },
          ),
          ProfileMenu(
          icon: Icons.settings, 
          text: 'Settings', 
          press: () { 
             Navigator.push(context,
             MaterialPageRoute(builder: (context)=> Settings()));
          },
          ),
          ProfileMenu(
          icon: Icons.help,
          text: 'Help', 
          press: () {
             Navigator.push(context,
             MaterialPageRoute(builder: (context)=> Help()));
           },
          ),
          ProfileMenu(
          icon: Icons.logout_outlined,
          text: 'Log out', 
          press: () { 
            FirebaseAuth.instance.signOut().then((value) {
               Navigator.push(context,
             MaterialPageRoute(builder: (context)=> Login()));
            });
            
          },
          ),

      ],
      
    );
  }
}

class ProfileMenu extends StatelessWidget {
  const ProfileMenu({
     required this.text, required this.icon, required this.press,
  });

  final String text;
  final IconData icon;
  final VoidCallback press;

  @override
  Widget build(BuildContext context) {
    return SingleChildScrollView(
      child: Padding(
        padding:  EdgeInsets.symmetric(
          horizontal: 20,
          vertical: 13,
        ),
        child: TextButton(
          style: ButtonStyle(
            backgroundColor: MaterialStateProperty.all<Color>( Theme.of(context).primaryColor,),
            
          ),
          onPressed: press ,
          child: Row(
            children:  [
              Icon(icon, color: Colors.white,),
              SizedBox(width: 20),
              Expanded(
                child: Text(
                  text, style: Theme.of(context).textTheme.headline4,
                ),
                ),
                Icon(Icons.arrow_forward_ios, color: Colors.white,),
          
            ],
          )),
      ),
    );
  }
}
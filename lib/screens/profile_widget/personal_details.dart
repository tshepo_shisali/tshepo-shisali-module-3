// ignore_for_file: prefer_const_constructors, prefer_const_literals_to_create_immutables

import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:med_it/screens/bottom.dart';

class Personal_details extends StatefulWidget {
  const Personal_details({Key? key}) : super(key: key);

  @override
  _Personal_detailsState createState() => _Personal_detailsState();
}

class _Personal_detailsState extends State<Personal_details> {
      CollectionReference users = FirebaseFirestore.instance.collection('users');

      TextEditingController full_name =  TextEditingController();
      TextEditingController age =  TextEditingController();
      TextEditingController phone_number =  TextEditingController();


      Future<void> addUser() async {
      // Call the user's CollectionReference to add a new user
      return users
          .doc('ABC123')
          .set({
            'full name': full_name.text,
            'age': age.text,
            'mobile number': phone_number.text, 
          })
          .then((value) => print("User Added"))
          .catchError((error) => print("Failed to add user: $error"));
    }

    

  @override
  Widget build(BuildContext context) {
    return  Scaffold(
       backgroundColor: Color.fromARGB(255, 3, 12, 43),
      appBar: AppBar(
        title: Text('Personal details'),
    
      ),
      body: Container(
        padding: EdgeInsets.only(left: 15, top: 20, right: 15),
        child: GestureDetector(
          onTap: (){
            FocusScope.of(context).unfocus();
          },
          child: ListView(
            children: [
             SizedBox(height: 30,),
             BuildTextField("Full Name and Surname", full_name),
              BuildTextField("Age", age),
               BuildTextField("Mobile number", phone_number),

               ElevatedButton(
                    style: ButtonStyle(
                      shape: MaterialStateProperty.all<RoundedRectangleBorder>(RoundedRectangleBorder(borderRadius: BorderRadius.circular(50) )),
                    backgroundColor: MaterialStateProperty.all<Color>(Theme.of(context).primaryColor),
                   ),
                   child: Text('Save Changes',
                     style:  Theme.of(context).textTheme.headline1,
                   ),

                   onPressed: addUser,
              ),
               
             
            ],
          ),
        )
        
      )
    );
  }
}

Widget BuildTextField(String lableText, TextEditingController controller){
  return Padding(
    padding: EdgeInsets.only(bottom: 30),
    child: TextField(
      controller: controller,
      decoration: InputDecoration(
        contentPadding: EdgeInsets.only(bottom: 5),
        labelText: lableText,
        floatingLabelBehavior: FloatingLabelBehavior.always,

      ),),
    );
}

Widget GetUserDetails(String documentId, String full_name, String age, String mobile_number){
  CollectionReference users = FirebaseFirestore.instance.collection('users');
    return FutureBuilder<DocumentSnapshot>(
      
      future: users.doc(documentId).get(),
      builder:
          (BuildContext context, AsyncSnapshot<DocumentSnapshot> snapshot) {

        if (snapshot.hasError) {
          return Text("Something went wrong");
        }

        if (snapshot.hasData && !snapshot.data!.exists) {
          return Text("Document does not exist");
        }

        if (snapshot.connectionState == ConnectionState.done) {
          Map<String, dynamic> data = snapshot.data!.data() as Map<String, dynamic>;
          return Text("Full Name: ${full_name}");
        }

        return Text("loading");
      },
    );
  }


import 'package:flutter/material.dart';



class ProfileListItem extends StatelessWidget {
  
  final String text;
  final bool hasNavigation;

  const ProfileListItem({
    
    
    required this.text,
    this.hasNavigation = true,
  });

  @override
  Widget build(BuildContext context) {
    return Container(
      height: 20,
      
      child: Row(
        children: <Widget>[
          
          SizedBox(width: 20),
          Text(
            this.text,
            
          ),
          Spacer(),
          if (this.hasNavigation)
            Icon(
              Icons.arrow_forward,
            ),
        ],
      ),
    );
  }
}
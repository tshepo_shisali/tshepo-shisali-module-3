// ignore_for_file: prefer_const_constructors

import 'package:flutter/material.dart';

class My_file extends StatefulWidget {
  const My_file({Key? key}) : super(key: key);

  @override
  _My_fileState createState() => _My_fileState();
}

class _My_fileState extends State<My_file> {
  @override
  Widget build(BuildContext context) {
    return  Scaffold(
       backgroundColor: Color.fromARGB(255, 3, 12, 43),
      appBar: AppBar(
        
        
        title: Text('My file'),
        backgroundColor: Colors.white24,

        actions: <Widget> [
          IconButton(
           icon: Icon(Icons.search, color: Color.fromARGB(255, 24, 216, 21),),
           iconSize: 30.0,
           onPressed: (){},
          ),
           IconButton(
           icon: Icon(Icons.account_circle_outlined, color: Color.fromARGB(255, 24, 216, 21),),
           iconSize: 30.0,
           onPressed: (){},
          ),
        ]
        
      ),
      body: Center(
          child: Text('patient file',
            style: TextStyle(
              fontSize: 30, fontWeight: FontWeight.bold,
              color: Colors.blue,
            ),)
      ),
    );
  }
}

// ignore_for_file: prefer_const_constructors, prefer_const_literals_to_create_immutables, camel_case_types, prefer_final_fields

import 'package:flutter/material.dart';
import 'package:med_it/screens/plan.dart';
import 'package:med_it/screens/profile.dart';

import 'cart.dart';
import 'consult.dart';
import 'home.dart';



class Bottom_navigator_page extends StatefulWidget {
   
  String email;
  Bottom_navigator_page({ Key? key, required this.email}) : super(key: key);
  @override
  State<Bottom_navigator_page> createState() => _Bottom_navigator_pageState();
}

class _Bottom_navigator_pageState extends State<Bottom_navigator_page> {

  
  
  int _selectedIndex = 0;
  static  List<Widget> _bottomNavOptions = <Widget>[
    Home(),
    Consult(),
    Plan(),
    Cart(),
    Profile(),
  ];


  void _onItemTapped(int index){
    setState(() {
      _selectedIndex = index;
    });
  }
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Center(
        child: _bottomNavOptions.elementAt(_selectedIndex),
      ),
      bottomNavigationBar: BottomNavigationBar(
        type: BottomNavigationBarType.fixed,
        currentIndex: _selectedIndex,
        selectedItemColor: Colors.blue,
        unselectedItemColor: Colors.white,
        onTap: _onItemTapped,
        backgroundColor: Colors.transparent,
        items: [
          BottomNavigationBarItem(
              icon: Icon(Icons.home),
          label: 'Home',
          ),
          BottomNavigationBarItem(
            icon: Icon(Icons.videocam_sharp),
            label: 'Consult',
          ),
          BottomNavigationBarItem(
            icon: Icon(Icons.calendar_today_rounded),
            label: 'Plan',
          ),
          BottomNavigationBarItem(
            icon: Icon(Icons.sentiment_satisfied_alt_rounded),
            label: 'Cart',
          ),
          BottomNavigationBarItem(
            icon: Icon(Icons.person),
            label: 'Profile',
          ),

        ],
      ),

    );
  }
}




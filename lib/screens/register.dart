
// ignore_for_file: prefer_const_literals_to_create_immutables, prefer_const_constructors, use_key_in_widget_constructors, avoid_print
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:med_it/screens/bottom.dart';
import 'package:med_it/screens/login.dart';

class Register extends StatefulWidget {
  const Register({ Key? key }) : super(key: key);

  @override
  State<Register> createState() => _RegisterState();
}

class _RegisterState extends State<Register> {
  bool _isHidden = true;
  CollectionReference users = FirebaseFirestore.instance.collection('users');

  TextEditingController email =  TextEditingController();
  TextEditingController cellphone_no =  TextEditingController();
  TextEditingController username =  TextEditingController();
  TextEditingController pass =  TextEditingController();
  TextEditingController re_pass =  TextEditingController();

   Future<void> addUser() async {
      // Call the user's CollectionReference to add a new user
      return users
          .doc(email.text)
          .set({
            'full name': username.text,
            'age': "",
            'mobile number': cellphone_no.text, 
          })
          .then((value) => print("User Added"))
          .catchError((error) => print("Failed to add user: $error"));
    }



  void _passwordView(){
    setState(() {
      _isHidden = !_isHidden;
    });
  }


  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        
        
        title: Text('Register', 
        style:  Theme.of(context).textTheme.headline3,),
        backgroundColor: Theme.of(context).primaryColor,),

      body: SingleChildScrollView(
        child: Padding(
          padding: const EdgeInsets.symmetric(
            horizontal: 90.0,
            vertical: 20.0,
          ),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
               SizedBox(height: 30, width: 20),
               Icon(Icons.person, size: 80, color: Colors.white,),
               Text('Welcome to med it', style:  Theme.of(context).textTheme.headline3,),
               Padding(padding: EdgeInsets.only(bottom: 20)),
               Text('Please enter the following', style:  Theme.of(context).textTheme.headline5,),
              Padding(padding: EdgeInsets.only(bottom: 15)),
               TextField(
               controller: email,
               style:Theme.of(context).textTheme.headline5,
                decoration: InputDecoration(
                labelText: 'Email',
                labelStyle: Theme.of(context).textTheme.headline3,
                 border: OutlineInputBorder(),
                 ),
               ),
               TextField(
               controller: cellphone_no,
                 style: Theme.of(context).textTheme.headline5,
                decoration: InputDecoration(
                labelText: 'CellPhone number',
                labelStyle:  Theme.of(context).textTheme.headline3,
               // fillColor: Colors.red.withOpacity(0.7),
                 border:  OutlineInputBorder( /*borderRadius: BorderRadius.circular(20.0), borderSide: BorderSide(width: 0, style: BorderStyle.none)**/),

                 ),
               ),
               TextField(
               controller: username,
                 style: Theme.of(context).textTheme.headline5,
                decoration: InputDecoration(
                labelText: 'Username',
                labelStyle:  Theme.of(context).textTheme.headline3,
                 border: OutlineInputBorder()
                 ),
               ),
               TextField(
               controller: pass,

                 style: Theme.of(context).textTheme.headline5,
               obscureText: _isHidden,
                decoration: InputDecoration(
                labelText: 'password',
                suffix: InkWell(
                  onTap: _passwordView,
                  child: Icon(
                    _isHidden
                    ? Icons.visibility
                    : Icons.visibility_off,
                    ), ),
                labelStyle:  Theme.of(context).textTheme.headline3,
                 border: OutlineInputBorder()
                 ),
                 
               ),
               
                Padding(padding: EdgeInsets.only(bottom: 20)),
                
               Container(
                width: MediaQuery.of(context).size.width,
                height: 50.0,
                margin: const EdgeInsets.fromLTRB(0, 50, 0, 28),
                decoration: BoxDecoration(borderRadius: BorderRadius.circular(50)),
                 child: ElevatedButton(
                    style: ButtonStyle(
                      shape: MaterialStateProperty.all<RoundedRectangleBorder>(RoundedRectangleBorder(borderRadius: BorderRadius.circular(50) )),
                    backgroundColor: MaterialStateProperty.all<Color>(Theme.of(context).primaryColor),
                   ),
                   child: Text('Register',
                     style:  Theme.of(context).textTheme.headline1,
                   ),

                   onPressed: (){
                    FirebaseAuth.instance.createUserWithEmailAndPassword(
                      email: email.text, password: pass.text).then((value) {
                        addUser();
                        Navigator.push(context,
                         MaterialPageRoute(builder: (context)=> Bottom_navigator_page(email: username.toString())));
                      }).onError((error, stackTrace){
                        print("error ${error.toString()}");
                      });
                     

                   },
              ),
               ),
               Padding(padding: EdgeInsets.only(bottom: 30)),
                 ],
          ),
        ),
      ),

    
    );
  }
}
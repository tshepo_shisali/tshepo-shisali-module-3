// ignore_for_file: prefer_const_constructors

import 'package:flutter/material.dart';

class Plan extends StatefulWidget {
  const Plan({Key? key}) : super(key: key);

  @override
  _PlanState createState() => _PlanState();
}

class _PlanState extends State<Plan> {
  @override
  Widget build(BuildContext context) {
    return  Scaffold(
      body: Center(
          child: Text('My plan',
            style: TextStyle(
              fontSize: 30, fontWeight: FontWeight.bold,
              color: Colors.blue,
            ),)
      ),
    );
  }
}

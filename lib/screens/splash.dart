
// ignore_for_file: prefer_const_constructors, prefer_typing_uninitialized_variables

import 'dart:async';

import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:med_it/screens/login.dart';
import 'package:animated_splash_screen/animated_splash_screen.dart';



class SplashScreen extends StatefulWidget {
  const SplashScreen({ Key? key }) : super(key: key);

  @override
  State<SplashScreen> createState() => _SplashScreenState();
}

class _SplashScreenState extends State<SplashScreen> {
  bool showLoading = false;
  var _timer;


  @override
  void initState(){
    super.initState();
    _timer = Timer(
      Duration(seconds: 3),
       () => {
          showLoading = true, setState(() {
            
          },)
       });
       _navigatetologin();

  }

  _navigatetologin() async{
      await Future.delayed(Duration(seconds: 5),(){});
      Navigator.pushReplacement(context, MaterialPageRoute(
        builder: (context) => Login() ));
  }

  @override
  void dispose() {
    _timer.cancel();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      
      body: AnimatedSplashScreen(
        
      splash: Image.asset('assets/image2.png',
      
       fit: BoxFit.fill,),
      nextScreen: Login(),
      backgroundColor: Theme.of(context).scaffoldBackgroundColor,
      
      duration: 40000,
      splashIconSize: 200.0,
      splashTransition: SplashTransition.rotationTransition,
          
        ),
        
      );
      
    
  }
}